package com.tim.ticd.db;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import com.tim.ticd.model.Accommodation;
import com.tim.ticd.model.Booking;
import com.tim.ticd.model.Customer;

public class DB
{
	private static Connection con;

	public DB() throws Exception
	{	 
		Class.forName("org.sqlite.JDBC");
		Bundle bundle = FrameworkUtil.getBundle(this.getClass());
		URL dbURL = FileLocator.toFileURL(FileLocator.find(bundle, new Path("db/ticd.db"),null));
		con = DriverManager.getConnection("jdbc:sqlite:"+dbURL.getPath());	   
	}

	public ArrayList<Customer> getAllCustomers() throws SQLException
	{
		ArrayList<Customer> allCustomers= new ArrayList<Customer>();
		Customer c = new Customer();
		Statement request = con.createStatement();
		String sql = "SELECT * FROM Customer ORDER BY surname";
		System.out.println(sql);
		ResultSet result = request.executeQuery(sql);
		while (result.next())
		{
			c = createCustomer(true, result);
			allCustomers.add(c);
		}
		request.close();
		return allCustomers;
	}

	public void addCustomer(Customer c) throws SQLException
	{
		Statement request = con.createStatement();
		Calendar cal = c.getDateOfBirth();
		String date = String.valueOf(cal.get(Calendar.DATE)) + "-" + String.valueOf(cal.get(Calendar.MONTH)+1) + "-" + String.valueOf(cal.get(Calendar.YEAR));
		String sql = "INSERT INTO Customer (title, firstname, surname, property, street, city," +
				"postcode, phone, mobile, email, dateofbirth) VALUES " + "('"
				+ c.getTitle() + "', '"
				+ c.getFirstName() + "', '"
				+ c.getSurname() + "', '"
				+ c.getProperty() + "', '"
				+ c.getStreet() + "', '"
				+ c.getCity() + "', '"
				+ c.getPostcode() + "', '"
				+ c.getPhone() + "', '"
				+ c.getMobile() + "', '"
				+ c.getEmail() + "', '"
				+ date + "')";
		System.out.println(sql);
		request.executeUpdate(sql);
		request.close();
	}

	public void setCustomer(Customer c) throws SQLException
	{
		Statement anfrage = con.createStatement();
		Calendar cal = c.getDateOfBirth();
		String date = String.valueOf(cal.get(Calendar.DATE)) + "-" + String.valueOf(cal.get(Calendar.MONTH)+1) + "-" + String.valueOf(cal.get(Calendar.YEAR));
		String sql = "UPDATE Customer SET "
				+ "title = '" + c.getTitle()
				+ "', firstname = '" + c.getFirstName() 
				+ "', surname = '" + c.getSurname() 
				+ "', property = '" + c.getProperty() 
				+ "', street = '" + c.getStreet() 
				+ "', city = '" + c.getCity() 
				+ "', postcode = '" + c.getPostcode() 
				+ "', phone = '" + c.getPhone() 
				+ "', mobile = '" + c.getMobile() 
				+ "', email = '" + c.getEmail() 
				+ "', dateofbirth = '" + date + "' "              
				+ "WHERE id = " + c.getID();
		System.out.println(sql);
		anfrage.executeUpdate(sql);
		anfrage.close();
	}

	public Customer getCustomerByID(int ID) throws SQLException{
		Customer cust = new Customer();
		Statement query = con.createStatement();
		String sql = "SELECT * FROM Customer WHERE ID='" + ID + "'";
		ResultSet result = query.executeQuery(sql);
		while(result.next()){
			cust = createCustomer(false, result);
		}
		query.close();
		return cust;
	}

	public Customer getLatestCustomer() throws SQLException{
		Customer cust = new Customer();
		Statement query = con.createStatement();
		String sql = "SELECT * FROM Customer WHERE id IN (SELECT max(id) FROM Customer)";
		ResultSet result = query.executeQuery(sql);
		while(result.next()){
			cust = createCustomer(true, result);
		}
		query.close();
		return cust;
	}

	public void removeCustomer(Customer c) throws SQLException
	{
		Statement anfrage = con.createStatement();
		String sql = "DELETE FROM Customer " + "WHERE id = " + c.getID();
		System.out.println(sql);
		anfrage.executeUpdate(sql);
		anfrage.close();
	}

	/**
	 * Supplementary method to avoid repeating code
	 * @param withID decide whether or not to fetch ID from database
	 * @param result the ResultSet from the query
	 * @return Customer object with data from database
	 * @throws SQLException
	 */
	private Customer createCustomer(boolean withID, ResultSet result) throws SQLException{
		Customer cust = new Customer();
		if(withID){
			cust.setID(result.getInt("ID"));
		}
		cust.setTitle(result.getString("title"));
		cust.setFirstName(result.getString("firstname"));
		cust.setSurname(result.getString("surname"));
		cust.setProperty(result.getString("property"));
		cust.setStreet(result.getString("street"));
		cust.setCity(result.getString("city"));
		cust.setPostcode(result.getString("postcode"));
		cust.setPhone(result.getString("phone"));
		cust.setMobile(result.getString("mobile"));
		cust.setEmail(result.getString("email"));
		Calendar dateofbirth = Calendar.getInstance();
		String date = result.getString("dateofbirth");
		System.out.println(date);
		dateofbirth.set(Calendar.DATE, Integer.valueOf(date.substring(0,2)));
		dateofbirth.set(Calendar.MONTH, Integer.valueOf(date.substring(3,5))-1);
		dateofbirth.set(Calendar.YEAR, Integer.valueOf(date.substring(6,10)));
		cust.setDateOfBirth(dateofbirth);
		return cust;
	}
	
	// ========================= ACCOMMODATION =================================== //

	public ArrayList<Accommodation> getAllAccommodations() throws SQLException
	{
		ArrayList<Accommodation> allAccommodations= new ArrayList<Accommodation>();
		Statement request = con.createStatement();
		String sql = "SELECT * FROM Accommodation ORDER BY id";
		System.out.println(sql);
		ResultSet result = request.executeQuery(sql);
		while (result.next())
		{
			int id = result.getInt("id");
			String name = result.getString("name");
			String street = result.getString("street");
			String city = result.getString("city");
			String postcode = result.getString("postcode");
			String phone = result.getString("phone");
			String email = result.getString("email");
			String website = result.getString("website");
			int rooms = result.getInt("rooms");
			float price = result.getFloat("price");
			String image = result.getString("image");
			//hier fehlen noch die facilities, keine Ahnung wie man da drankommt?
			Accommodation a = new Accommodation(id, name, street, city, postcode,
					phone, email, website, rooms, price, image, null);
			allAccommodations.add(a);
		}
		request.close();
		return allAccommodations;
	}

	public ArrayList<Booking> getAllBookings() throws SQLException
	{
		ArrayList<Booking> allBookings= new ArrayList<Booking>();
		Statement request = con.createStatement();
		String sql = "SELECT * FROM Booking ORDER BY id";
		System.out.println(sql);
		ResultSet result = request.executeQuery(sql);
		while (result.next())
		{
			int id = result.getInt("id");
			int cid = result.getInt("customerID");
			int accid = result.getInt("accommodationID");
			String startdate = result.getString("startdate");
			String enddate = result.getString("enddate");
			Booking b = new Booking(id, cid, accid, startdate, enddate);
			allBookings.add(b);
		}
		request.close();
		return allBookings;
	}

	public void addBooking(Booking b) throws SQLException
	{
		SimpleDateFormat formatierer = new SimpleDateFormat("yyyy-MM-dd");
		Statement request = con.createStatement();
		String sql = "INSERT INTO Booking (ID, customerID, accommodationID, startdate, enddate) VALUES " + "('"
				+ b.getID() + "', '"
				+ b.getCustomerID() + "', '"
				+ b.getAccommodationID() + "', '"
				+ formatierer.format(b.getStartdate()) + "', '"
				+ formatierer.format(b.getEnddate()) + "')";
		System.out.println(sql);
		request.executeUpdate(sql);
		request.close();
	}

	public void setBooking(Booking b) throws SQLException
	{
		SimpleDateFormat formatierer = new SimpleDateFormat("yyyy-MM-dd");
		Statement anfrage = con.createStatement();
		String sql = "UPDATE Booking SET " + "customerID = '"
				+ b.getCustomerID() + "', accommodationID = '"
				+ b.getAccommodationID() + "', startdate = '"
				+ formatierer.format(b.getStartdate()) + "', enddate = '"
				+ formatierer.format(b.getEnddate()) + "' "                 
				+ "WHERE id = " + b.getID();
		System.out.println(sql);
		anfrage.executeUpdate(sql);
		anfrage.close();
	}

	public void removeBooking(Booking b) throws SQLException
	{
		Statement anfrage = con.createStatement();
		String sql = "DELETE FROM Booking " + "WHERE id = " + b.getID();
		System.out.println(sql);
		anfrage.executeUpdate(sql);
		anfrage.close();
	}

}
