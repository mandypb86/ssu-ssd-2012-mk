package com.tim.ticd.model;


public class Booking {
	
	private int ID;
	private int customerID;
	private int accommodationID;
	private String startdate;
	private String enddate;
	
	public Booking(int ID, int cID, int accID, String sd, String ed) {
		this.ID = ID;
		this.customerID = cID;
		this.accommodationID = accID;
		this.startdate = sd;
		this.enddate = ed;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public int getAccommodationID() {
		return accommodationID;
	}

	public void setAccommodationID(int accomodationID) {
		this.accommodationID = accomodationID;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	

}
