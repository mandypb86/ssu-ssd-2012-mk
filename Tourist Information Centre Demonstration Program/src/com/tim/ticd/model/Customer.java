package com.tim.ticd.model;

import java.util.Calendar;

public class Customer {
	
	private int ID;
	private String title;
	private String firstname;
	private String surname;
	private String property;
	private String street;
	private String city;
	private String postcode;
	private String phone;
	private String mobile;
	private String email;
	private Calendar dateofbirth;
	
	// empty constructor needed for Customer-Wizard
	public Customer(){};
	
	public Customer (int ID, String t, String n, String sn, String p, String st,
			String c, String pc, String ph, String m, String e, Calendar dob) {
		this.ID = ID;
		this.title = t;
		this.firstname = n;
		this.property = p;
		this.street = st;
		this.city = c;
		this.postcode = pc;
		this.surname = sn;
		this.mobile = m;
		this.phone = ph;	
		this.email = e;
		this.dateofbirth = dob;
		
	}
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getTitle(){
		return title;
	}
	public void setTitle(String title){
		this.title = title;
	}
	public String getFirstName() {
		return firstname;
	}
	public void setFirstName(String name) {
		this.firstname = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Calendar getDateOfBirth() {
		return dateofbirth;
	}
	public void setDateOfBirth(Calendar dateOfBirth) {
		this.dateofbirth = dateOfBirth;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	

}
