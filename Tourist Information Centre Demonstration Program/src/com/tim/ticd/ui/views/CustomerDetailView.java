package com.tim.ticd.ui.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import com.tim.ticd.model.Customer;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.core.databinding.UpdateValueStrategy;
import com.tim.ticd.ui.wizards.converter.SimpleTextToCalendarConverter;
import com.tim.ticd.ui.wizards.converter.SimpleCalendarToTextConverter;

public class CustomerDetailView extends ViewPart {
	private DataBindingContext m_bindingContext;
	private Text textFirstNames;
	private Text textFamilyName;
	private Text textDateOfBirth;
	private Text textFlatProperty;
	private Text textStreet;
	private Text textCity;
	private Text textPostcode;
	private Text textPhoneHome;
	private Text textPhoneMobile;
	private Text textEmail;
	
	private Customer cust;

	public CustomerDetailView() {
	}
	
	public void setCustomer(Customer cust){
		this.cust = cust;
	}

	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(5, false));
		
		Label lblFirstNames = new Label(composite, SWT.NONE);
		lblFirstNames.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFirstNames.setText("First name/s");
		
		textFirstNames = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
		GridData gd_textFirstNames = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_textFirstNames.widthHint = 200;
		textFirstNames.setLayoutData(gd_textFirstNames);
		new Label(composite, SWT.NONE);
		
		Label lblDateOfBirth = new Label(composite, SWT.NONE);
		lblDateOfBirth.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDateOfBirth.setText("Date of birth");
		
		textDateOfBirth = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
		textDateOfBirth.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblFamilyName = new Label(composite, SWT.NONE);
		lblFamilyName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFamilyName.setText("Family name");
		
		textFamilyName = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
		GridData gd_textFamilyName = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_textFamilyName.widthHint = 200;
		textFamilyName.setLayoutData(gd_textFamilyName);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		
		Group grpAddress = new Group(composite, SWT.NONE);
		grpAddress.setLayout(new GridLayout(2, false));
		grpAddress.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));
		grpAddress.setText("Address");
		
		Label lblFlatNumber = new Label(grpAddress, SWT.NONE);
		lblFlatNumber.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFlatNumber.setText("Flat number /\r\nProperty name");
		
		textFlatProperty = new Text(grpAddress, SWT.BORDER | SWT.READ_ONLY);
		textFlatProperty.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblStreet = new Label(grpAddress, SWT.NONE);
		lblStreet.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblStreet.setText("Street");
		
		textStreet = new Text(grpAddress, SWT.BORDER | SWT.READ_ONLY);
		textStreet.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblCity = new Label(grpAddress, SWT.NONE);
		lblCity.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCity.setText("City");
		
		textCity = new Text(grpAddress, SWT.BORDER | SWT.READ_ONLY);
		textCity.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblPostcode = new Label(grpAddress, SWT.NONE);
		lblPostcode.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPostcode.setText("Postcode");
		
		textPostcode = new Text(grpAddress, SWT.BORDER | SWT.READ_ONLY);
		textPostcode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(composite, SWT.NONE);
		
		Group grpPhoneAndEmail = new Group(composite, SWT.NONE);
		grpPhoneAndEmail.setLayout(new GridLayout(2, false));
		grpPhoneAndEmail.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));
		grpPhoneAndEmail.setText("Phone and Email");
		
		Label lblHome = new Label(grpPhoneAndEmail, SWT.NONE);
		lblHome.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblHome.setText("Home");
		
		textPhoneHome = new Text(grpPhoneAndEmail, SWT.BORDER | SWT.READ_ONLY);
		textPhoneHome.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblMobile = new Label(grpPhoneAndEmail, SWT.NONE);
		lblMobile.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMobile.setText("Mobile");
		
		textPhoneMobile = new Text(grpPhoneAndEmail, SWT.BORDER | SWT.READ_ONLY);
		textPhoneMobile.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblEmail = new Label(grpPhoneAndEmail, SWT.NONE);
		lblEmail.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEmail.setText("Email");
		
		textEmail = new Text(grpPhoneAndEmail, SWT.BORDER | SWT.READ_ONLY);
		textEmail.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		
		Group grpInquieryHistory = new Group(composite, SWT.NONE);
		grpInquieryHistory.setLayout(new FillLayout(SWT.HORIZONTAL));
		grpInquieryHistory.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
		grpInquieryHistory.setText("Inquiry History");
		
		List list = new List(grpInquieryHistory, SWT.BORDER);
		m_bindingContext = initDataBindings();
		
	}

	@Override
	public void setFocus() {
		
	}
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue observeTextTextFirstNamesObserveWidget = WidgetProperties.text(SWT.Modify).observe(textFirstNames);
		IObservableValue firstNameCustObserveValue = PojoProperties.value("firstName").observe(cust);
		bindingContext.bindValue(observeTextTextFirstNamesObserveWidget, firstNameCustObserveValue, null, null);
		//
		IObservableValue observeTextTextDateOfBirthObserveWidget = WidgetProperties.text(SWT.Modify).observe(textDateOfBirth);
		IObservableValue dateOfBirthCustObserveValue = PojoProperties.value("dateOfBirth").observe(cust);
		UpdateValueStrategy strategy = new UpdateValueStrategy();
		strategy.setConverter(new SimpleTextToCalendarConverter());
		UpdateValueStrategy strategy_1 = new UpdateValueStrategy();
		strategy_1.setConverter(new SimpleCalendarToTextConverter());
		bindingContext.bindValue(observeTextTextDateOfBirthObserveWidget, dateOfBirthCustObserveValue, strategy, strategy_1);
		//
		IObservableValue observeTextTextFamilyNameObserveWidget = WidgetProperties.text(SWT.Modify).observe(textFamilyName);
		IObservableValue surnameCustObserveValue = PojoProperties.value("surname").observe(cust);
		bindingContext.bindValue(observeTextTextFamilyNameObserveWidget, surnameCustObserveValue, null, null);
		//
		IObservableValue observeTextTextFlatPropertyObserveWidget = WidgetProperties.text(SWT.Modify).observe(textFlatProperty);
		IObservableValue propertyCustObserveValue = PojoProperties.value("property").observe(cust);
		bindingContext.bindValue(observeTextTextFlatPropertyObserveWidget, propertyCustObserveValue, null, null);
		//
		IObservableValue observeTextTextStreetObserveWidget = WidgetProperties.text(SWT.Modify).observe(textStreet);
		IObservableValue streetCustObserveValue = PojoProperties.value("street").observe(cust);
		bindingContext.bindValue(observeTextTextStreetObserveWidget, streetCustObserveValue, null, null);
		//
		IObservableValue observeTextTextCityObserveWidget = WidgetProperties.text(SWT.Modify).observe(textCity);
		IObservableValue cityCustObserveValue = PojoProperties.value("city").observe(cust);
		bindingContext.bindValue(observeTextTextCityObserveWidget, cityCustObserveValue, null, null);
		//
		IObservableValue observeTextTextPostcodeObserveWidget = WidgetProperties.text(SWT.Modify).observe(textPostcode);
		IObservableValue postcodeCustObserveValue = PojoProperties.value("postcode").observe(cust);
		bindingContext.bindValue(observeTextTextPostcodeObserveWidget, postcodeCustObserveValue, null, null);
		//
		IObservableValue observeTextTextPhoneHomeObserveWidget = WidgetProperties.text(SWT.Modify).observe(textPhoneHome);
		IObservableValue phoneCustObserveValue = PojoProperties.value("phone").observe(cust);
		bindingContext.bindValue(observeTextTextPhoneHomeObserveWidget, phoneCustObserveValue, null, null);
		//
		IObservableValue observeTextTextPhoneMobileObserveWidget = WidgetProperties.text(SWT.Modify).observe(textPhoneMobile);
		IObservableValue mobileCustObserveValue = PojoProperties.value("mobile").observe(cust);
		bindingContext.bindValue(observeTextTextPhoneMobileObserveWidget, mobileCustObserveValue, null, null);
		//
		IObservableValue observeTextTextEmailObserveWidget = WidgetProperties.text(SWT.Modify).observe(textEmail);
		IObservableValue emailCustObserveValue = PojoProperties.value("email").observe(cust);
		bindingContext.bindValue(observeTextTextEmailObserveWidget, emailCustObserveValue, null, null);
		//
		return bindingContext;
	}
}
