package com.tim.ticd.ui.views;

import java.sql.SQLException;
import java.util.Calendar;

import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.part.ViewPart;

import com.tim.ticd.db.DB;
import com.tim.ticd.model.Customer;

public class CustomerListView extends ViewPart {
	private DB db;
	private Table table;
	private TableViewer tableViewer;
	
	public CustomerListView() {
		try {
			db = new DB();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void createPartControl(Composite parent) {
		
		Composite composite = new Composite(parent, SWT.NONE);
		TableColumnLayout tcl_composite = new TableColumnLayout();
		composite.setLayout(tcl_composite);
		
		tableViewer = new TableViewer(composite, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnId = tableViewerColumn.getColumn();
		tcl_composite.setColumnData(tblclmnId, new ColumnPixelData(30, true, true));
		tblclmnId.setText("ID");
		
		TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnFamilyName = tableViewerColumn_1.getColumn();
		tcl_composite.setColumnData(tblclmnFamilyName, new ColumnPixelData(150, true, true));
		tblclmnFamilyName.setText("Family Name");
		
		TableViewerColumn tableViewerColumn_2 = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnFirstName = tableViewerColumn_2.getColumn();
		tcl_composite.setColumnData(tblclmnFirstName, new ColumnPixelData(150, true, true));
		tblclmnFirstName.setText("First Name");
		
		TableViewerColumn tableViewerColumn_3 = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnDateOfBirth = tableViewerColumn_3.getColumn();
		tcl_composite.setColumnData(tblclmnDateOfBirth, new ColumnPixelData(150, true, true));
		tblclmnDateOfBirth.setText("Date of Birth");
		tableViewer.setContentProvider(new ArrayContentProvider());
		tableViewer.setLabelProvider(new CustomerTableLabelProvider());
		
		try {
			tableViewer.setInput(db.getAllCustomers());
		} catch (SQLException e) {
			System.out.println("Customers could not be fetched from database.");
			e.printStackTrace();
		}

	}

	@Override
	public void setFocus() {
		table.setFocus();
	}
	
	public TableViewer getTableViewer(){
		return tableViewer;
	}
	
	class CustomerTableLabelProvider implements ITableLabelProvider{

		@Override
		public void addListener(ILabelProviderListener listener) {
			// do nothing
		}

		@Override
		public void dispose() {
			// do nothing
		}

		@Override
		public boolean isLabelProperty(Object element, String property) {
			return false;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {
			// do nothing
		}

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			Customer cust = (Customer) element;
			switch(columnIndex){
			case 0: return String.valueOf(cust.getID());
			case 1: return cust.getSurname();
			case 2: return cust.getFirstName();
			case 3: Calendar cal = cust.getDateOfBirth();
					System.out.println(String.valueOf(cal.get(Calendar.YEAR)));
					return String.valueOf(cal.get(Calendar.DATE)) + "-" + String.valueOf(cal.get(Calendar.MONTH)+1) + "-" + String.valueOf(cal.get(Calendar.YEAR));				
			default: return "";
			}
		}
		
	}

}
