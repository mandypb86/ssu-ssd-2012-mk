package com.tim.ticd.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.tim.ticd.model.Customer;
import com.tim.ticd.ui.wizards.converter.SimpleCalendarToTextConverter;
import com.tim.ticd.ui.wizards.converter.SimpleTextToCalendarConverter;
import com.tim.ticd.ui.wizards.validators.NotEmptyValidator;

public class CustomerGeneralInformationWizardPage extends WizardPage {
	private DataBindingContext m_bindingContext;
	
	private Customer cust;
	
	private Composite container;
	private Text textFirstName;
	private Text textFamilyName;
	private Text textDateOfBirth;
	private Combo cmbTitle;
	
	protected CustomerGeneralInformationWizardPage(Customer p_cust){
		super("Customer General Information");
		setTitle("General Information");
		setMessage("Please provide some general information about the new customer.");
		this.cust = p_cust;
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.None);
		setControl(container);
		container.setLayout(new GridLayout(2, false));
		
		Label lblTitle = new Label(container, SWT.NONE);
		lblTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTitle.setText("Title");
		
		cmbTitle = new Combo(container, SWT.NONE);
		cmbTitle.setItems(new String[] {"Mr", "Mrs", "Miss", "Ms"});
		
		Label lblFirstName = new Label(container, SWT.NONE);
		lblFirstName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFirstName.setText("First name/s");
		
		textFirstName = new Text(container, SWT.BORDER);
		textFirstName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblFamilyName = new Label(container, SWT.NONE);
		lblFamilyName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFamilyName.setText("Family name");
		
		textFamilyName = new Text(container, SWT.BORDER);
		textFamilyName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblDateOfBirth = new Label(container, SWT.NONE);
		lblDateOfBirth.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDateOfBirth.setText("Date of birth");
		
		textDateOfBirth = new Text(container, SWT.BORDER);
		textDateOfBirth.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		m_bindingContext = initDataBindings();
		
	}
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue observeTextCmbTitleObserveWidget = WidgetProperties.text().observe(cmbTitle);
		IObservableValue titleCustObserveValue = PojoProperties.value("title").observe(cust);
		bindingContext.bindValue(observeTextCmbTitleObserveWidget, titleCustObserveValue, null, null);
		//
		IObservableValue observeTextTextFirstNameObserveWidget = WidgetProperties.text(SWT.Modify).observe(textFirstName);
		IObservableValue firstNameCustObserveValue = PojoProperties.value("firstName").observe(cust);
		UpdateValueStrategy strategy_2 = new UpdateValueStrategy();
		strategy_2.setAfterConvertValidator(new NotEmptyValidator());
		bindingContext.bindValue(observeTextTextFirstNameObserveWidget, firstNameCustObserveValue, strategy_2, null);
		//
		IObservableValue observeTextTextFamilyNameObserveWidget = WidgetProperties.text(SWT.Modify).observe(textFamilyName);
		IObservableValue surnameCustObserveValue = PojoProperties.value("surname").observe(cust);
		UpdateValueStrategy strategy_3 = new UpdateValueStrategy();
		strategy_3.setAfterConvertValidator(new NotEmptyValidator());
		bindingContext.bindValue(observeTextTextFamilyNameObserveWidget, surnameCustObserveValue, strategy_3, null);
		//
		IObservableValue observeTextTextDateOfBirthObserveWidget = WidgetProperties.text(SWT.Modify).observe(textDateOfBirth);
		IObservableValue dateOfBirthCustObserveValue = PojoProperties.value("dateOfBirth").observe(cust);
		UpdateValueStrategy strategy = new UpdateValueStrategy();
		strategy.setConverter(new SimpleTextToCalendarConverter());
		UpdateValueStrategy strategy_1 = new UpdateValueStrategy();
		strategy_1.setConverter(new SimpleCalendarToTextConverter());
		bindingContext.bindValue(observeTextTextDateOfBirthObserveWidget, dateOfBirthCustObserveValue, strategy, strategy_1);
		//
		return bindingContext;
	}
}
