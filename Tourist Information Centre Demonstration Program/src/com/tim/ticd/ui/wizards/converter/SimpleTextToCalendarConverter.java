package com.tim.ticd.ui.wizards.converter;

import java.util.Calendar;

import org.eclipse.core.databinding.conversion.Converter;

public class SimpleTextToCalendarConverter extends Converter {

	public SimpleTextToCalendarConverter(){
		super(String.class, Calendar.class);
	}
	
	public SimpleTextToCalendarConverter(String fromType, Calendar toType) {
		super(fromType, toType);
	}

	@Override
	public Object convert(Object fromObject) {
		String date = (String)fromObject;
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, Integer.valueOf(date.substring(0, 2)));
		cal.set(Calendar.MONTH, Integer.valueOf(date.substring(3, 5))-1);
		cal.set(Calendar.YEAR, Integer.valueOf(date.substring(6,10)));
		return cal;
	}

}
