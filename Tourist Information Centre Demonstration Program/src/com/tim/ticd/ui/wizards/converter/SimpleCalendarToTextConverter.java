package com.tim.ticd.ui.wizards.converter;

import java.util.Calendar;

import org.eclipse.core.databinding.conversion.Converter;

public class SimpleCalendarToTextConverter extends Converter {

	public SimpleCalendarToTextConverter(){
		super(Calendar.class, String.class);
	}
	
	public SimpleCalendarToTextConverter(Calendar fromType, String toType) {
		super(fromType, toType);
	}

	@Override
	public Object convert(Object fromObject) {
		Calendar cal = (Calendar)fromObject;
		
		return cal.get(Calendar.DATE) + "-" + (cal.get(Calendar.MONTH)+1) + "-" + cal.get(Calendar.YEAR);
	}

}
