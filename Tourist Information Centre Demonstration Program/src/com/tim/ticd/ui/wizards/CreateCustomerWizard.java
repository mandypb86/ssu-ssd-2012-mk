package com.tim.ticd.ui.wizards;

import java.sql.SQLException;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.PlatformUI;

import com.tim.ticd.db.DB;
import com.tim.ticd.model.Customer;
import com.tim.ticd.ui.views.CustomerListView;

public class CreateCustomerWizard extends Wizard {
	
	private Customer cust = new Customer();
	private DB db;

	public CreateCustomerWizard(){
		setWindowTitle("Create Customer Wizard");
		addPage(new CustomerGeneralInformationWizardPage(cust));
		addPage(new CustomerContactInformationWizardPage(cust));
		
		try {
			db = new DB();
		} catch (Exception e) {
			System.out.println("Database connection could not be created");
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean performFinish() {
		try {
			db.addCustomer(cust);
			CustomerListView customerListView = (CustomerListView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView("com.tim.ticd.ui.views.customerListView");
			customerListView.getTableViewer().setInput(db.getAllCustomers());
		} catch (SQLException e) {
			System.out.println("Customer could not be saved into database");
			e.printStackTrace();
			return false;
		}
		return true; // creation was successful, close wizard
	}
}
