package com.tim.ticd.ui.wizards.validators;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public class NotEmptyValidator implements IValidator {
	
	public NotEmptyValidator(){
		super();

	}

	@Override
	public IStatus validate(Object value) {
		if(value instanceof String){
			String text = (String) value;
			if(text.trim().length() == 0){
				return Status.CANCEL_STATUS;
			}
		}
		return Status.OK_STATUS;
	}

}
